(function($) {
	Drupal.behaviors.dynamic_rules_behavior_apply = {
		attach : function(context) {
			var data = $(context).data('openlayers');
			if (data && data.map.behaviors.dynamic_rules_behavior_apply) {

				/**
				 * Here we create a new style object with rules that determine
				 * which symbolizer will be used to render each feature.
				 */
				var style = new OpenLayers.Style();
				
				var rules1 = new OpenLayers.Rule({
						// a rule contains an optional filter
						filter : new OpenLayers.Filter.Comparison({
							type : OpenLayers.Filter.Comparison.BETWEEN,
							property : "field_rule_count", // the "foo" feature attribute
							lowerBoundary : 0,
							upperBoundary : 5
						}),
						// if a feature matches the above filter, use this symbolizer
						symbolizer : {
							//orange
                            fillColor: "#ff771f",
                            strokeColor: "#ffffff",
                            strokeWidth: 2,
						}
					});
				
				var rules2 = new OpenLayers.Rule({
						filter : new OpenLayers.Filter.Comparison({
							type : OpenLayers.Filter.Comparison.BETWEEN,
							property : "field_rule_count",
							lowerBoundary : 5,
							upperBoundary : 10
						}),
						symbolizer : {
							//yellow
                            fillColor: "#ffd81f",
                            strokeColor: "#ffffff",
                            strokeWidth: 2,
						}
					});
				
				var rules3 = new OpenLayers.Rule({
						filter : new OpenLayers.Filter.Comparison({
							type : OpenLayers.Filter.Comparison.BETWEEN,
							property : "field_rule_count",
							lowerBoundary : 10,
							upperBoundary : 15
						}),
						symbolizer : {
							//green
                            fillColor: "#54ff1f",
                            strokeColor: "#ffffff",
                            strokeWidth: 2,
						}
					});
				
				var rules4 = new OpenLayers.Rule({
						// apply this rule if no others apply
						elseFilter : true,
						symbolizer : {
							//skyblue
                            fillColor: "#1ff7ff",
                            strokeColor: "#ffffff",
                            strokeWidth: 2
						}
					});
				
				style.addRules([rules1,rules2,rules3,rules4]);
				
				var map_layers = data.openlayers.layers;
				var behavior_layers = data.map.behaviors.dynamic_rules_behavior_apply.layers;
				
				for(var index = 0; index<map_layers.length; index++){
					if(map_layers[index].drupalID == behavior_layers[map_layers[index].drupalID]) {
						map_layers[index].styleMap.styles.default = style;
						map_layers[index].redraw();
					}
				}

			}
		}
	};

})(jQuery);
